#include "stdio.h"
#include "stdlib.h"
#include "libs/ulibs.h"
#include "string.h"

// Function signatures
void childProcess(int fd1[], int fd2[], int duration);

// Global variables
int running = 1;

// Message structure with two possible interpretations: string or numbers
union message {
    struct {
        char s[8];
    };
    struct {
        double a;
        double b;
    } n;
};

// Response structure with two possible interpretations: string or calculated values
union response {
    struct {
        char s[8];
    };
    struct {
        double sum;
        double difference;
        double product;
        double division;
    } n;
};

int main() {
    pid_t pid;
    int status, fd1[2], fd2[2], executionTime;
    time_t t0, t1, n;
    union message buf;
    union response resp;

    time(&t0);

    printf("Parent: PID %d\n", getpid());

    // Create communication pipes
    pipe(fd1);
    pipe(fd2);
    printf("Communication pipe created\n");

    // Prompt user for execution time
    printf("Enter execution time for the child process (in seconds): ");
    scanf("%d", &executionTime);

    // Fork the child process
    pid = fork();

    // If the process is the child, call the childProcess function
    if (pid == 0) {
        childProcess(fd1, fd2, executionTime);
        return 0;
    // If the process is the parent, create a new one
    } else {
        printf("New child process created: PID %d\n", pid);
    }

    // Close unnecessary pipe ends
    close(fd1[1]);
    close(fd2[0]);

    while (strcmp(buf.s, "END") != 0) {
        // Read message from the parent
        n = read(fd1[0], (void *)&buf, sizeof(buf));

        if (running == 0) {
            // If running is 0, terminate the child process if "END" message is received
            if (strcmp(buf.s, "END") == 0) {
                strcpy(resp.s, "END OK");
                break;
            }
        } else {
            // Calculate the sum, difference, product, and division
            resp.n.sum = buf.n.a + buf.n.b;
            resp.n.difference = buf.n.a - buf.n.b;
            resp.n.division = buf.n.a / buf.n.b;
            resp.n.product = buf.n.a * buf.n.b;
        }

        // Send response back to the parent
        write(fd2[1], (void *)&resp, sizeof(resp));
    }

    // Wait for the child process to finish
    if ((pid = wait(&status)) > 0) {
        printf("\nProcess %d has finished\n", pid);
        // Close all pipe ends
        close(fd1[0]);
        close(fd1[1]);
        close(fd2[0]);
        close(fd2[1]);
    }

    time(&t1);
    printf("Total time: %ld sec\n", t1 - t0);

    return 0;
}

// Child process function
void childProcess(int fd1[], int fd2[], int duration) {
    time_t t0, t1;
    int i = 0;
    char buf[80];

    union message child;
    union response resp;

    running = 1;
    // Close unnecessary pipe ends
    close(fd1[0]);
    close(fd2[1]);

    srand(time(&t0));

    while (running == 1) {
        if (i == duration) {
            running = 0;
            strcpy(child.s, "END");
        } else {
            // Generate random input values for child process
            child.n.a = rand() % 35;
            child.n.b = rand() % 35;
            printf("\n------------------------------------------");
            printf("\nChild random input:  a:%.1lf     b:%.1lf   \n", child.n.a, child.n.b);
        }

        time(&t0);
        t1 = t0;

        // Send message to the parent
        write(fd1[1], (void *)&child, sizeof(child));
        // Receive response from the parent
        read(fd2[0], (void *)&resp, sizeof(resp));

        if (strcmp(resp.s, "END OK") != 0 && running == 1) {
            // Print the calculated values if not terminating
            printf("\nResponse:\n");
            printf("    Sum:           %.1lf + %.1lf = %.1lf\n", child.n.a, child.n.b, resp.n.sum);
            printf("    Difference:    %.1lf - %.1lf = %.1lf\n", child.n.a, child.n.b, resp.n.difference);
            printf("    Division:      %.1lf / %.1lf = %.1lf\n", child.n.a, child.n.b, resp.n.division);
            printf("    Product:       %.1lf * %.1lf = %.1lf\n\n", child.n.a, child.n.b, resp.n.product);
            printf("\n------------------------------------------");
            i++;
        } else {
            break;
        }

        // Wait for the remaining duration
        while ((t1 - t0) < (duration / duration)) {
            time(&t1);
        }
    }
}
