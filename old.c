#include <stdio.h>
#include <stdlib.h>
#include "libs/ulibs.h"

void hijo(int n, int fd[]);

int main()
{
  pid_t pid;
  int status, i, j, fd[5][2], n;
  time_t t0, t1;
  char buf[80];

  time(&t0);

  printf("Padre: PID %d\n", getpid());

  // Crea 5 procesos hijos, luego espera un mensaje de cada uno
  // y finalmente espera a que finalicen

  for(i=0; i<5; ++i)
  {
    pipe(fd[i]);  // Crea el pipe para la comunicación con el hijo i-ésimo

    pid = fork(); // Crea el hijo <i>

    if( pid==0 )
    {
      hijo(i, fd[i]);
      return 0;
    } else {
      // El padre continua por acá

      printf("Nuevo proceso creado: PID %d\n", pid);
      close(fd[i][1]);
    }
  }

  // Espera recibir dos mensajes de cada hijo a través del pipe
  for(j=0; j<2; ++j)
  {
    for(i=0; i<5; ++i)
    {
      n = read(fd[i][0], buf, 80);

      printf("Rx de hijo[%d] << %s >> - (%d bytes)\n", i, buf, n);
    }
  }

  // Espera por la finalización de cada hijo

  for(i=0; i<5; ++i)
  {
    if( (pid = wait(&status)) > 0 )
    {
      printf("Finalizó proceso %d\n",pid);
      close(fd[i][0]);
      close(fd[i][1]);
    }
  }
  time(&t1);
  printf("Tiempo total: %d\n", t1-t0);

  return 0;
}

//----------------------------------------------
// Función que ejecuta cada proceso hijo
// Permanece consumiendo tiempo de CPU durante
// un cierto tiempo y luego finaliza
//----------------------------------------------

void hijo(int n, int fd[])
{
  volatile time_t t0, t1;
  int nbytes;
  char buf[80];

  close(fd[0]);  // Cierra el extremo de recepción (no usado)

  time(&t0);
  t1 = t0;

  sprintf(buf, "Inicia hijo %d - PID %d\n", n, getpid());

  write(fd[1], buf, strlen(buf)+1);

  while( (t1-t0) < 15 )
  {
    time(&t1);
  }

  sprintf(buf, "Finaliza hijo %d - PID %d - tiempo ejecución: %d s\n", n, getpid(), t1-t0);

  write(fd[1], buf, strlen(buf)+1);
}
